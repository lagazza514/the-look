package look.look

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth

class MainActivity : AppCompatActivity() {

    val firebaseauther = FirebaseAuth.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val logbtn = findViewById<View>(R.id.login_button) as Button
        val regbtn = findViewById<View>(R.id.register_button) as Button

        logbtn.setOnClickListener {
            login()
        }
        regbtn.setOnClickListener {
            register()
        }
    }

    private fun login(){
        val emailtxt = findViewById<View>(R.id.email) as EditText
        val passtxt = findViewById<View>(R.id.password) as EditText
        var email = emailtxt.text.toString()
        var password = passtxt.text.toString()

        if(!email.isEmpty() && !password.isEmpty()){
            firebaseauther.signInWithEmailAndPassword(email, password).addOnCompleteListener(this) { task ->
                if(task.isSuccessful){
                    startActivity(Intent(this, Main2Activity :: class.java))
                    Toast.makeText(this, "logged in", Toast.LENGTH_SHORT).show()
                }else{
                    Toast.makeText(this, "error", Toast.LENGTH_SHORT).show()
                }
            }
        }else{
            Toast.makeText(this, "error", Toast.LENGTH_SHORT).show()
        }
    }
    private fun register(){
        val emailtxt = findViewById<View>(R.id.email) as EditText
        val passtxt = findViewById<View>(R.id.password) as EditText
        var email = emailtxt.text.toString()
        var password = passtxt.text.toString()

        if(!email.isEmpty() && !password.isEmpty()){
            firebaseauther.createUserWithEmailAndPassword(email, password).addOnCompleteListener(this) { task ->
                if(task.isSuccessful){
                    Toast.makeText(this, "registered!", Toast.LENGTH_SHORT).show()
                }else if(password.length < 6) {
                    Toast.makeText(this, "password needs to be at least 6 long", Toast.LENGTH_LONG).show()
                }else{
                    Toast.makeText(this, "error", Toast.LENGTH_SHORT).show()
                }
            }
        }else{
            Toast.makeText(this, "error", Toast.LENGTH_SHORT).show()
        }
    }
}
