package look.look

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import com.squareup.picasso.Picasso

class Main2Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        var startimg = findViewById<ImageView>(R.id.images)

        Picasso.get().load(R.mipmap.ic_launcher).into(startimg)

        firstim()
        secondim()
        thirdim()
        fourthim()

    }

   private fun firstim(){
       var theimg1 = findViewById<ImageView>(R.id.images)
       var btn1 = findViewById<Button>(R.id.img1)
        btn1.setOnClickListener {
            Picasso.get().load("https://i.imgur.com/PNpGUvq.jpg?1").error(R.mipmap.ic_launcher).resize(300, 300).centerCrop().into(theimg1)
        }
    }
   private fun secondim(){
       var theimg2 = findViewById<ImageView>(R.id.images)
       var btn2 = findViewById<Button>(R.id.img2)
        btn2.setOnClickListener {
            Picasso.get().load("https://i.imgur.com/MY1uxtQ.jpg").error(R.mipmap.ic_launcher).resize(300, 300).centerCrop().into(theimg2)
        }
    }
   private fun thirdim(){
       var theimg3 = findViewById<ImageView>(R.id.images)
       var btn3 = findViewById<Button>(R.id.img3)
        btn3.setOnClickListener {
            Picasso.get().load("https://i.imgur.com/Uzs39BH.jpg").error(R.mipmap.ic_launcher).resize(300, 300).centerCrop().into(theimg3)
        }
    }
   private fun fourthim(){
       var theimg4 = findViewById<ImageView>(R.id.images)
       var btn4 = findViewById<Button>(R.id.img4)
        btn4.setOnClickListener {
            Picasso.get().load("https://i.imgur.com/R6UWdIe.jpg").error(R.mipmap.ic_launcher).resize(300, 300).centerCrop().into(theimg4)
        }
    }

}
